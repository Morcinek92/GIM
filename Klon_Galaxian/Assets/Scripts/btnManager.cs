﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class btnManager : MonoBehaviour
{
    public GUIText daneText;
    
    private bool flaga;
    public string stringToEdit;

    private void Start()
    { 
        flaga = false;
        daneText.text = "";
        stringToEdit = "";
    }

    public void NowaGraBtn(string NowaGra)
    {
        flaga = true;
    }

    private void OnGUI()
    {
        if (flaga == true)
        { 
            stringToEdit = GUI.TextField(new Rect(Screen.width * 38 / 100, Screen.height * 43 / 100 + 10, 200, 30), stringToEdit, 30);

            if ( stringToEdit.Length > 1)
            {
                GUILayout.Space(290);
               
                if (GUI.Button(new Rect(Screen.width * 44 / 100, Screen.height * 50 / 100 + 10, 100, 30), "Graj !"))
                {
                    PlayerPrefs.SetString("name", stringToEdit);
                    SceneManager.LoadScene("main");
                }
            } else
            {
                daneText.text = "Wprowadź nazwę gracza";                
            } 
        }
    }

    public void Wyjdzbtn()
    {
        PlayerPrefs.Save();
        Application.Quit();
    }

    public void RankingBtn(string ranking)
    {
        SceneManager.LoadScene(ranking);        
    }


}

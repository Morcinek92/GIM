﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject hazard;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public GUIText scoreText;
    public GUIText restartText;
    public GUIText gameOverText;
    public GUIText menuText;

    public int score;
    private bool gameOver;
    private bool restart;
    private bool pauza;
    private AudioSource[] pausedSources;

    void Start()
    {
        pauza = false;
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        menuText.text = "";
        score = 0;
        UpdateScore();
        StartCoroutine(SpawnWaves());
    }

    private void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene("Main");
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("Menu");
            }
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (pauza)
            {
                Time.timeScale = 1;
                gameOverText.text = "";
                pauza = false;
                foreach (AudioSource source in pausedSources)
                {
                    if (source)
                    {
                        source.Play();
                    }
                }
            }
            else
            {   // Pauza 
                Time.timeScale = 0;
                gameOverText.text = "Pauza !";
                pauza = true;
                // Wyciszanie dźwięku 
                AudioSource[] aSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
                pausedSources = new AudioSource[aSources.Length];
                for (int i = 0; i < aSources.Length; i++)
                {
                    AudioSource source = aSources[i];
                    if (source.isPlaying)
                    {
                        source.Pause();
                        pausedSources[i] = source;
                    }
                }
            }
        }

    }
    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                restartText.text = "Naciśnij 'Enter' żeby ponowić grę";
                menuText.text = "Powrót do menu wciśnij 'ESC'";
                PlayerPrefs.SetFloat("score", score);
                HighScoreManager._instance.SaveHighScore(PlayerPrefs.GetString("name"), (int)PlayerPrefs.GetFloat("score"));
                restart = true;
                break;
            }
        }
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Punkty: " + score;
    }

    public void GameOver()
    {
        gameOverText.text = "Koniec Gry !";
        gameOver = true;
    }
}

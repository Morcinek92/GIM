﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public float Predkosc;

    private void Start()
    {
        GetComponent<Rigidbody>().velocity = Vector3.forward * Predkosc;
    }
}

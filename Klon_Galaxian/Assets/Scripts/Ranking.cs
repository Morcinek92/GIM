﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ranking : MonoBehaviour
{
    private bool gui;
    new string name = "";
    new string score = "";

    List<Scores> highscore;

    void Start()
    {
        gui = true;
        highscore = new List<Scores>();
        highscore = HighScoreManager._instance.GetHighScore();
    }

    public void Menu(string menu)
    {
        gui = false;
        SceneManager.LoadScene(menu);
    }

    public void wyczysc()
    {
        if (gui == true)
        {
            HighScoreManager._instance.ClearLeaderBoard();
        }
    }

    private void OnGUI()
    {
        if (gui == true)
        {
            GUILayout.Space(200);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Gracz", GUILayout.Width(Screen.width / 2));
            GUILayout.Label("Punkty", GUILayout.Width(Screen.width / 2));
            GUILayout.EndHorizontal();

            GUILayout.Space(25);

            foreach (Scores _score in highscore)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(_score.name, GUILayout.Width(Screen.width / 2));
                GUILayout.Label("" + _score.score, GUILayout.Width(Screen.width / 2));
                GUILayout.EndHorizontal();
            }
        }
    }
}